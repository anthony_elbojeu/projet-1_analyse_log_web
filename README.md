# Projet_1-Groupe_2

Membres du groupe : Anthony, Gabriel

# Description des fichiers du répertoire

Le fichier "pipeline.sh" contient un script avec les commandes du terminal qui permettent: 
-Le téléchargement du fichier log, 
-Son extraction du fichier log, 
-L'appel au script Python,
-L'appel aux scripts SQL,
-La suppression des fichiers temporaires

Le fichier "fichier_CSV.py" contient un script Python qui importe le fichier log téléchargé au format csv.

Le fichier "load.sql" contient le script pour l'importation du fichier csv dans la base de données.

Le fichier "reponses.sql" contient un script avec les requêtes permettant de répondre à la problématique.

# Plus d'explication sur le fonctionnement des différents scripts et l'approche choisi pour répondre à la problématique

https://docs.google.com/presentation/d/1pRL-wu25gdkaLvKTNOLqlgOVROhPYbrMBGJUqRXdWCc/edit?usp=sharing

# Références

Pour réaliser des schémas explicatifs:
https://excalidraw.com/

Pour tester des regex:
https://regex101.com/

Pour extraire une sous-chaîne dans SQLite:
https://www.developpez.net/forums/d804450/bases-donnees/autres-sgbd/sqlite/strftime-question-blob/

Comment détecter les navigateurs utilisés par les internautes depuis un fichier log:
https://developer.mozilla.org/fr/docs/Web/HTTP/Detection_du_navigateur_en_utilisant_le_user_agent