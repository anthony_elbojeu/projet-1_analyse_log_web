-- 1. Specifier le mode CSV et le séparateur à utiliser avant l'import SQLite




-- 2. Importer le fichier CSV "data.csv" dans la table "raw_data"

drop table if exists raw_data;
CREATE TABLE IF NOT EXISTS raw_data(adresse_ip TEXT, user_identifier TEXT, name TEXT , datetexte TEXT, fuseauhoraire TEXT, request TEXT, status TEXT, objectsize TEXT, referer TEXT, user_agent TEXT, browser TEXT, system_exploitation TEXT, appareil TEXT, autre TEXT);

.mode csv
--.header on
.separator ","
.import accessSQL.csv raw_data

