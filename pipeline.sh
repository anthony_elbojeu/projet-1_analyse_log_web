
#Attribution des valeurs

DATA_URL="https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J"
DATA_FILE=raw_data.zip
DATABASE_FILE=ecommerce.db

##############################################################################
# Partie 1 : extraction de données
##############################################################################

echo "Téléchargement du dataset ${DATA_FILE} (${DATA_URL})"

wget -qO $DATA_FILE $DATA_URL
unzip raw_data.zip
mv access.log access.log.txt



echo "Création du fichier .csv grâce au script fichier_CSV"
python3 fichier_CSV.py

echo "Script fichier_CSV terminé!"

##############################################################################
# Partie 2 : chargement de données
##############################################################################

echo "Chargement des données en base ${DATABASE_FILE} (load.sql)"

sqlite3 $DATABASE_FILE < load.sql


##############################################################################
# Partie 3 : Traitement des données
##############################################################################

echo "Réponses aux questions sur la base de données ${DATABASE_FILE} (reponses.sql)"

sqlite3 $DATABASE_FILE < reponses.sql

##############################################################################
# nettoyage des fichiers temporaires
##############################################################################

echo "Nettoyage des fichiers temporaires"
echo "Suppression de l'archive raw_data.zip"

rm raw_data.zip

echo "Suppression du fichier de log"

rm access.log.txt

echo "Suppression du fichier converti"

rm accessSQL.csv

echo "Suppression du fichier __MACOSX"
rm -Rf __MACOSX

